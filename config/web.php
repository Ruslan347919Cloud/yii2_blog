<?php
use yii\helpers\Html;

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'language' => 'en',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'pnjs2wz7pjM5XOO4gitRgnSjI3Z_9HGp',
            'baseUrl' => ''
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['auth/login']
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'viewPath' => '@app/mail',
            'htmlLayout' => 'layouts/main-html',
            'textLayout' => 'layouts/main-text',
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => ['admin@test.com' => 'Admin'],
            ],
            'useFileTransport' => true,
            /*'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'admin',
                'password' => 'admin',
                'port' => '465',
                'encryption' => 'SSL',
            ],*/
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            //'cache' => 'cache'
        ],
        'socialShare' => [
            'class' => \ymaker\social\share\configurators\Configurator::class,
            'socialNetworks' => [
                'facebook' => [
                    'class' => \ymaker\social\share\drivers\Facebook::class,
                    'label' => Html::tag('i', '', [
                        'class' => 'fa fa-facebook-square fa-2x',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'right',
                        'title' => 'Facebook',
                        'aria-hidden' => 'true'
                    ]),
                    'options' => ['class' => 'fb'],
                ],
                'vkontakte' => [
                    'class' => \ymaker\social\share\drivers\Vkontakte::class,
                    'label' => Html::tag('i', '', [
                        'class' => 'fa fa-vk fa-2x',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'right',
                        'title' => 'Vkontakte',
                        'aria-hidden' => 'true'
                    ]),
                    'options' => ['class' => 'vk'],
                ],
                /*'twitter' => [
                    'class' => \ymaker\social\share\drivers\Twitter::class,
                    'label' => Yii::t('app', 'Twitter'),
                    'options' => ['class' => 'tw'],
                    'config' => [
                        'account' => $params['twitterAccount']
                    ],
                ],*/
                // ...
            ],
            'options' => [
                'class' => 'social-network',
            ],
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\Module',
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
