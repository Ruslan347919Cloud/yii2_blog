<?php
	use yii\helpers\Html;
	use yii\helpers\StringHelper;
	use yii\helpers\Url;

	$this->title = 'Search result';
?>

<?php if (empty($foundArticles) && empty($foundUsers) && empty($foundComments)):?>
	<p>No results</p>
<?php else:?>

	<?php if (!empty($foundArticles)):?>
		<h4>Articles:</h4>
		<ul>
			<?php foreach ($foundArticles as $article):?>
				<li>
					<a href="<?=Url::to(['site/view', 'id' => $article->id])?>">
						<span>
							<?=Html::encode($article->title)?>
						</span>
						<small>
							(author: <?=Html::encode($article->author->name)?>)
						</small>
					</a>
				</li>
			<?php endforeach;?>
		</ul>
	<?php endif;?>

	<?php if (!empty($foundUsers)):?>
		<h4>Users:</h4>
		<ul>
			<?php foreach ($foundUsers as $user):?>
				<li>
					<?=Html::encode($user->name)?>
				</li>
			<?php endforeach;?>
		</ul>
	<?php endif;?>

	<?php if (!empty($foundComments)):?>
		<h4>Comments:</h4>
		<ul>
			<?php foreach ($foundComments as $comment):?>
				<li>
					<div>
						<?=StringHelper::truncate($comment->text, 100)?>
					</div>
					<small>
						by <?=Html::encode($comment->user->name)?>.
					</small>
					<small>
						<a href="<?=Url::to(['site/view', 'id' => $comment->article->id])?>">
							Article: <?=Html::encode($comment->article->title)?>
						</a>
					</small>
				</li>
			<?php endforeach;?>
		</ul>
	<?php endif;?>

<?php endif;?>