<?php

namespace app\models;
use yii\data\Pagination;

use Yii;

/**
 * This is the model class for table "tag".
 *
 * @property int $id
 * @property string $title
 *
 * @property ArticleTag[] $articleTags
 */
class Tag extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tag';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles() {
        return $this->hasMany(Article::className(),
            ['id' => 'article_id'])
                ->viaTable('article_tag', ['tag_id' => 'id']);
    }

    public static function getAll() {
        return Tag::find()->with('articles')->all();
    }

    public static function getArticlesByTag($id, $pageSize = 3) {
        $query = Article::find()
            ->joinWith('tags')
            ->where(['tag.id' => $id]);
        $pagination = new Pagination([
            'defaultPageSize' => $pageSize,
            'totalCount' => $query->count()
        ]);
        $articles = $query
            ->offset($pagination->offset)
            ->limit($pagination->limit)->all();
        $data = [
            'articles' => $articles,
            'pagination' =>$pagination
        ];
        return $data;
    }
}
