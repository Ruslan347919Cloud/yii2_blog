<?php
    use yii\helpers\Url;
    use yii\helpers\StringHelper;
    use yii\helpers\Html;
?>
<!--Sidebar (popular, recent, categories list)-->
<div class="col-lg-4 col-md-5 sidebar">
    <!--Poular-->
    <div class="list-group popular mb-4">
        <h4 class="list-group-item bg-light">
            <i class="fa fa-star-o" aria-hidden="true"></i>
            <span>Popular posts</span>
        </h4>
        <?php foreach ($popularPosts as $popularPost):?>
            <a href="<?=Url::toRoute(['site/view', 'id' => $popularPost->id])?>" class="list-group-item list-group-item-action">
                <h5 class="d-flex justify-content-between">
                    <span>
                        <?=StringHelper::truncate($popularPost->title, 14)?>
                    </span>
                    <small>
                        <i class="fa fa-eye"></i>
                        <?=$popularPost->viewed?>
                    </small>
                </h5>
                <p class="mb-1">
                    <?=StringHelper::truncate($popularPost->content, 100)?>
                </p>
                <small><?=($popularPost->author) ? Html::encode($popularPost->author->name) : 'Unknown'?></small>
            </a>
        <?php endforeach;?>
    </div>
    <!--Recent-->
    <div class="list-group recent mb-4">
        <h4 class="list-group-item bg-light">
            <i class="fa fa-clock-o" aria-hidden="true"></i>
            <span>Recent posts</span>
        </h4>
        <?php foreach ($recentPosts as $recentPost):?>
            <a href="<?=Url::toRoute(['site/view', 'id' => $recentPost->id])?>" class="list-group-item list-group-item-action">
                <div class="d-flex justify-content-between">
                  <h5>
                    <?=StringHelper::truncate($recentPost->title, 14)?>
                  </h5>
                  <small><?=$recentPost->getDate()?></small>
                </div>
                <p class="mb-1">
                    <?=StringHelper::truncate($recentPost->content, 100)?>
                </p>
                <small><?=($recentPost->author) ? Html::encode($recentPost->author->name) : 'Unknown'?></small>
            </a>
        <?php endforeach;?>
    </div>
    <!--Categories-->
    <div class="list-group categories mb-4">
        <h4 class="list-group-item bg-light">
            <i class="fa fa-sticky-note-o" aria-hidden="true"></i>
            <span>Categories</span>
        </h4>
        <?php foreach ($categories as $category):?>
            <a href="<?=Url::toRoute(['site/category', 'id' => $category->id])?>" class="list-group-item list-group-item-action">
                <h5 class="d-flex justify-content-between">
                  <span>
                    <i class="fa fa-circle-thin" aria-hidden="true"></i>
                    <span><?=Html::encode($category->title)?></span>
                  </span>
                  <span class="badge badge-primary badge-pill">
                      <?=$category->getArticlesCount()?>
                  </span>
                </h5>
            </a>
        <?php endforeach;?>
    </div>
    <!--Tags-->
    <div class="list-group categories mb-4">
        <h4 class="list-group-item bg-light">
            <i class="fa fa-tags" aria-hidden="true"></i>
            <span>Tags</span>
        </h4>
        <?php foreach ($tags as $tag):?>
            <a href="<?=Url::toRoute(['site/tag', 'id' => $tag->id])?>" class="list-group-item list-group-item-action">
                <h5 class="d-flex justify-content-between">
                  <span><?=Html::encode($tag->title)?></span>
                  <span class="badge badge-primary badge-pill">
                      <?=count($tag->articles)?>
                  </span>
                </h5>
            </a>
        <?php endforeach;?>
    </div>
</div>