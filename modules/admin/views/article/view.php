<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Set image', ['set-image', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if (Yii::$app->user->can('manageArticle')):?>
            <?= Html::a('Set category', ['set-category', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Set tags', ['set-tags', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description:ntext',
            'content:ntext',
            'date',
            'viewed',
            'user_id',
            [
                'format' => 'html',
                'label' => 'Author',
                'value' => function($data) {
                    return ($data->author) ? $data->author->name : NULL;
                }
            ],
            'status',
            [
                'format' => 'html',
                'label' => 'Image',
                'value' => function($data) {
                    return Html::img($data->getImage(), ['width' => 300]);
                }
            ],
        ],
    ]) ?>
    
    <?php if ($model->categories):?>
        <h4>Related categories</h4>
        <ul>
            <?php foreach ($model->categories as $category):?>
                <li>
                    <?=Html::encode($category->title)?>
                </li>
            <?php endforeach;?>
        </ul>
    <?php else:?>
        <h4>No categories</h4>
    <?php endif;?>
    <?php if ($model->tags):?>
        <h4>Related tags</h4>
        <ul>
            <?php foreach ($model->tags as $tag):?>
                <li>
                    <?=Html::encode($tag->title)?>
                </li>
            <?php endforeach;?>
        </ul>
    <?php else:?>
        <h4>No tags</h4>
    <?php endif;?>

</div>
