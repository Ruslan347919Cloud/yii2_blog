<?php
	$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['auth/signup-confirm', 'token' => $emailConfirmToken]);
?>
Follow the link below to confirm your email:
<?= $confirmLink ?>