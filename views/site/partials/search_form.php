<!--Search form-->
<form action="/site/search" method="get">
	<div class="input-group">
	  <input type="text" name="searchQuery" class="form-control" placeholder="search..." aria-label="search" aria-describedby="button-addon">
	  <div class="input-group-append">
	    <button class="btn btn-outline-secondary" type="submit" id="button-addon">
	    	<i class="fa fa-search" aria-hidden="true"></i>
	    </button>
	  </div>
	</div>
</form>