<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
use app\models\Article;
use app\models\Category;
use app\models\Comment;
use app\models\Tag;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SiteSearch;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class SiteController extends Controller {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout() {
        return $this->render('about');
    }

    public function actionView($id = NULL) {
        $commentModel = new Comment;
        $tags = Tag::getAll();
        $categories = Category::getAll();
        $popularPosts = Article::getPopularPosts();
        $recentPosts = Article::getRecentPosts();
        $currentArticle = Article::find()
            ->with('author', 'tags')
            ->where(['id' => $id])->one();
        $currentArticle->updateCounters(['viewed' => 1]);
        $commentsData = Comment::getAllForThisArticle(
            $currentArticle->id, 4);
        $comments = $commentsData['comments'];
        $pagination = $commentsData['pagination'];
        return $this->render('single_post', compact(
            'comments', 'currentArticle','popularPosts',
            'pagination', 'recentPosts', 'categories',
            'tags', 'commentModel')
        );
    }

    public function actionComment($id) {
        $commentModel = new Comment($id);
        if ($commentModel->load(Yii::$app->request->post())
            && $commentModel->save()) {
            Yii::$app->session->setFlash('comment', 'Your comment was sent to server. It will appear here soon.');
            $this->redirect(['site/view', 'id' => $id]);
        }
    }

    public function actionCategory($id = NULL) {
        $tags = Tag::getAll();
        $popularPosts = Article::getPopularPosts();
        $recentPosts = Article::getRecentPosts();
        $categories = Category::getAll();
        $data = Category::getArticlesByCategory($id);
        $articles = $data['articles'];
        $pagination = $data['pagination'];

        return $this->render('index', compact('articles',
            'popularPosts', 'recentPosts', 'categories', 'tags', 'pagination'));
    }

    public function actionTag($id = NULL) {
        $tags = Tag::getAll();
        $popularPosts = Article::getPopularPosts();
        $recentPosts = Article::getRecentPosts();
        $categories = Category::getAll();
        $data = Tag::getArticlesByTag($id);
        $articles = $data['articles'];
        $pagination = $data['pagination'];

        return $this->render('index', compact('articles',
            'popularPosts', 'recentPosts', 'categories',
                'tags', 'pagination'));
    }

    public function actionSearch($searchQuery) {
        $data = SiteSearch::search($searchQuery);
        $foundArticles = $data['articles'];
        $foundUsers = $data['users'];
        $foundComments = $data['comments'];

        return $this->render('search', compact(
            'foundArticles', 'foundUsers', 'foundComments'));
    }
    
    public function actionIndex() {
        $data = Article::getAll();
        $articles = $data['articles'];
        $pagination = $data['pagination'];
        $tags = Tag::getAll();
        $popularPosts = Article::getPopularPosts();
        $recentPosts = Article::getRecentPosts();
        $categories = Category::getAll();
        
        return $this->render('index', compact('articles',
            'pagination', 'popularPosts', 'recentPosts', 'tags', 'categories'));
    }
}
