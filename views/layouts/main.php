<?php

use yii\helpers\Html;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-type" content="text/html; charset=<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody() ?>
        <!--Header with navbar and logo-->
        <nav class="navbar navbar-expand-sm mb-4 navbar-light border bg-light">
            <a href="<?=Url::toRoute(['site/index'])?>" class="navbar-brand">
                My Blog
            </a>
            <button class="navbar-toggler">
                <span class="navbar-toggler-icon" data-toggle="collapse" data-target="#navbar-menu"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-menu">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item pr-md-2 active">
                        <a href="<?=Url::toRoute(['site/index'])?>" class="nav-link">Home</a>
                    </li>
                    <?php if (Yii::$app->user->can('admin')):?>
                        <li class="nav-item pr-md-2">
                            <a href="<?=Url::toRoute(['admin/default/index'])?>" class="nav-link">Admin</a>
                        </li>
                    <?php endif;?>
                    <?php if (Yii::$app->user->isGuest):?>
                        <li class="nav-item pr-md-2">
                            <a href="<?=Url::toRoute(['auth/login'])?>" class="nav-link">Login</a>
                        </li>
                        <li class="nav-item pr-md-2">
                            <a href="<?=Url::toRoute(['auth/signup'])?>" class="nav-link">Register</a>
                        </li>
                    <?php else:?>
                        <li>
                            <a href="<?=Url::toRoute(['/admin/user/view', 'id' => Yii::$app->user->id])?>" class="nav-link">
                                <?='My Profile ('.Yii::$app->user->identity->name.')'?>
                            </a>
                        </li>
                        <li>
                            <a href="<?=Url::toRoute(['/auth/logout'])?>" class="nav-link">
                                Logout
                            </a>
                        </li>
                    <?php endif;?>
                    <li class="nav-item pr-md-2">
                        <a href="<?=Url::toRoute(['site/about'])?>" class="nav-link">About</a>
                    </li>
                    <li class="nav-item pr-md-2">
                        <?=$this->render('/site/partials/search_form')?>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container-fluid wrapper">
            <?= $content ?>
        </div>
        <!--Footer-->
        <footer class="container-fluid bg-secondary">
            <div class="row py-1 align-items-center justify-content-between text-center">
                <div class="col-sm-6 col-md-3">&copy; MyBlog.com</div>
                <a href="tel:#" class="col-sm-6 col-md-3">Tel: 000-000-000</a>
                <a href="mailto:#" class="col-sm-6 col-md-3">
                    E-mail: example@somemail.com
                </a>
                <div class="col-sm-6 col-md-2">
                    <a href="#">
                        <i class="fa fa-facebook-official fa-2x" aria-hidden="true"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-twitter-square fa-2x" aria-hidden="true"></i>
                    </a>
                    <a href="#">
                        <i class="fa fa-vk fa-2x" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </footer>

        <!--Content: Posts with pagination-->

        <!--Page: Post -> Full text with tags and comments-->

        <!--Page: Register ant Authentification-->

        <!--CMS-->
        
    <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>