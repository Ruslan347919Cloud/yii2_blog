<?php
namespace app\models;

use yii\base\Model;

class SiteSearch extends Model {
	public $searchQuery;

	public function rules() {
		return [
			[['searchQuery'], 'string'],
			[['searchQuery'], 'required']
		];
	}

	public static function search($searchQuery) {
		$query = trim($searchQuery);
    $articles = Article::find()
      ->orWhere(['like', 'content', $query])
      ->orWhere(['like', 'title', $query])
      ->orWhere(['like', 'description', $query])
      ->with('author')
      ->limit(10)
      ->all();
    $users = User::find()
      ->where(['like', 'name', $query])
      ->limit(10)
      ->all();
    $comments = Comment::find()
      ->where(['like', 'text', $query])
      ->with('user', 'article')
      ->limit(10)
      ->all();
    return compact('articles', 'users', 'comments');
	}
}