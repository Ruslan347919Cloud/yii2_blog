<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $photo
 *
 * @property Comment[] $comments
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(['access_token' => $token]);
    }

    public function getId() {
        return $this->id;
    }

    public function getAuthKey() {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey) {
        return $this->getAuthKey() === $authKey;
    }

    public static function findByEmail($email) {
        return User::find()->where(['email' => $email])->one();
    }

    public static function findByUsername($username) {
        return static::findOne(['name' => $username]);
    }

    public function validatePassword($password) {
        return ($this->password === $password) ? TRUE : FALSE;
    }

    public function create() {
        return $this->save(FALSE);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'password', 'photo'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'E-Mail',
            'password' => 'Password',
            'photo' => 'Photo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments() {
        return $this->hasMany(Comment::className(), ['user_id' => 'id']);
    }

    public function getArticles() {
        return $this->hasMany(Article::className(), ['user_id' => 'id']);
    }

    private function getUserRoles() {
        return Yii::$app->authManager->getRolesByUser($this->id);
    }

    private function assignDefaultRole() {
        if (count($this->getUserRoles()) === 0) {
            $auth = Yii::$app->authManager;
            $author = $auth->getRole('author');
            $auth->assign($author, $this->id);
            return TRUE;
        }
        return FALSE;
    }

    private function deleteUserRoles() {
        $auth = Yii::$app->authManager;
        $auth->revokeAll($this->id);
    }

    private function deleteUserArticles() {
        $userArticles = $this->articles;
        foreach ($userArticles as $article)
            $article->delete();
    }

    public function saveImage($fileName) {
        $this->photo = $fileName;
        return $this->save(FALSE);
    }

    public function getImage() {
        return ($this->photo) ? Url::base(true).'/uploads/'.$this->photo : Url::base(true).'/no_image.jpg';
    }

    public function deleteImage() {
        $imageUploadModel = new ImageUpload();
        $imageUploadModel->deleteCurrentImage($this->photo);
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
            }
            return TRUE;
        }
        return FALSE;
    }

    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        $this->assignDefaultRole();
    }

    public function beforeDelete() {
        if (!parent::beforeDelete())
            return FALSE;
        $this->deleteImage();
        $this->deleteUserRoles();
        $this->deleteUserArticles();
        return TRUE;
    }

    public function sendMail($view, $subject, $params = []) {
        $mailer = Yii::$app->mailer;
        $mailer->getView()->params['name'] = $this->name;
        $result = $mailer->compose([
            'html' => 'views/'.$view.'-html',
            'text' => 'views/'.$view.'-text',
        ], $params)->setTo([$this->email => $this->name])
            ->setSubject($subject)
            ->send();
        $mailer->getView()->params['name'] = null;
        return $result;
    }
}
