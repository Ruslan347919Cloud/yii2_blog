<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%role}}`.
 */
class m190903_080717_drop_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('{{%role}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->createTable('{{%role}}', [
            'id' => $this->primaryKey(),
            'title' => $this->varchar(32)->notNull()->unique(),
            'description' => $this->varchar(255),
        ]);
    }
}
