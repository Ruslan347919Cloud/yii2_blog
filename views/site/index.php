<?php

use yii\helpers\Url;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
use yii\helpers\Html;

$this->title = 'My Yii Blog';
?>

<div class="row">
    <?php if (Yii::$app->session->hasFlash('success')):?>
         <div class="alert alert-success alert-dismissible col-12" role="alert">
             <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
             <?php echo Yii::$app->session->getFlash('success'); ?>
         </div>
    <?php endif;?>
    <!--Posts-->
    <div class="col-lg-8 col-md-7">
        <?php foreach ($articles as $article):?>
            <div class="card post mb-4">
                <a href="<?=Url::toRoute(['site/view', 'id' => $article->id])?>">
                    <img src="<?=$article->getImage()?>" alt="" class="card-img-top">
                </a>
                <div class="card-body">
                    <h4 class="card-title"><?=Html::encode($article->title)?></h4>
                    <p class="card-text">
                        <?=StringHelper::truncate($article->content, 300)?>
                    </p>
                    <a href="<?=Url::toRoute(['site/view', 'id' => $article->id])?>" class="card-link">Read full text</a>
                </div>
                <div class="container-fluid card-footer text-muted">
                    <div class="row justify-content-between">
                        <span class="col-lg-5 col-md-7 col-sm-10">
                            By <?=($article->author) ? Html::encode($article->author->name) : 'Unknown'?>, 
                            <?=$article->getDate()?>
                        </span>
                        <span class="col-lg-2 col-md-3 col-sm-2">
                            <i class="fa fa-eye"></i>
                            <span> <?=(int)$article->viewed?></span>
                        </span>
                    </div>
                </div>
            </div>
        <?php endforeach;?>
    </div>
    <?=$this->render('partials/sidebar',
        compact('popularPosts', 'recentPosts', 'categories', 'tags'))?>
    <div class="col-12 pagination-block">
        <?=LinkPager::widget([
            'pagination' => $pagination,
            'activePageCssClass' => 'pagination-block__active',
            'linkOptions' => ['class' => 'pagination-block__link'],
            'disabledPageCssClass' => 'disabled'
        ])?>
    </div>
</div>
