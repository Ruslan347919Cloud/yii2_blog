<?php

namespace app\models;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\ImageUpload;
use yii\data\Pagination;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $date
 * @property string $image
 * @property int $viewed
 * @property int $user_id
 * @property int $status
 *
 * @property ArticleCategory[] $articleCategories
 * @property Category[] $categories
 * @property ArticleTag[] $articleTags
 * @property Comment[] $comments
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'article';
    }

    public function rules() {
        return [
            [['title'], 'required'],
            [['description', 'content'], 'string'],
            [['title', 'description', 'content'], 'trim'],
            [['date'], 'date', 'format' => 'php:Y-m-d'],
            [['date'], 'default', 'value' => date('Y-m-d')],
            [['user_id'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels() {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
            'date' => 'Date'
        ];
    }

    public function getTags() {
        return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('article_tag', ['article_id' => 'id']);
    }

    public function getCategories() {
        return $this->hasMany(Category::className(), ['id' => 'category_id'])->viaTable('article_category', ['article_id' => 'id']);
    }

    public function getComments() {
        return $this->hasMany(Comment::className(), ['article_id' => 'id']);
    }

    public function getAuthor() {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getSelectedTags() {
        $selectedIds = $this->getTags()
            ->select('id')->asArray()->all();
        return ArrayHelper::getColumn($selectedIds, 'id');
    }

    private function clearCurrentTags() {
        ArticleTag::deleteAll(['article_id' => $this->id]);
    }

    public function saveTags($tags) {
        if (is_array($tags)) {
            $this->clearCurrentTags();
            foreach ($tags as $tag_id) {
                $tag = Tag::findOne($tag_id);
                $this->link('tags', $tag);
            }
        }
    }

    public function getSelectedCategories() {
        $selectedIds = $this->getCategories()
            ->select('id')->asArray()->all();
        return ArrayHelper::getColumn($selectedIds, 'id');
    }

    private function clearCurrentCategories() {
        ArticleCategory::deleteAll(['article_id' => $this->id]);
    }

    public function saveCategories($categories) {
        if (is_array($categories)) {
            $this->clearCurrentCategories();
            foreach ($categories as $category_id) {
                $category = Category::findOne($category_id);
                $this->link('categories', $category);
            }
        }
    }

    public function saveImage($fileName) {
        $this->image = $fileName;
        return $this->save(FALSE);
    }

    public function getImage() {
        return ($this->image) ? Url::base(true).'/uploads/'.$this->image : Url::base(true).'/no_image.jpg';
    }

    public function deleteImage() {
        $imageUploadModel = new imageUpload();
        $imageUploadModel->deleteCurrentImage($this->image);
    }

    public function beforeDelete() {
        $this->deleteImage();
        return parent::beforeDelete();
    }

    public function getDate() {
        return Yii::$app->formatter->asDate($this->date);
    }

    public static function getAll($pageSize = 3) {
        $query = Article::find();
        $pagination = new Pagination([
            'defaultPageSize' => $pageSize,
            'totalCount' => $query->count()
        ]);
        $articles = $query->with('author')
            ->orderBy(['date' => SORT_DESC])
            ->offset($pagination->offset)
            ->limit($pagination->limit)->all();
        $data = [
            'articles' => $articles,
            'pagination' => $pagination
        ];
        return $data;
    }

    public static function getPopularPosts() {
        return Article::find()->with('author')
            ->orderBy(['viewed' => SORT_DESC])->limit(3)->all();
    }

    public static function getRecentPosts() {
        return Article::find()->with('author')
            ->orderBy(['date' => SORT_DESC])->limit(3)->all();
    }
}
