<?php
	use yii\helpers\Url;
	use yii\helpers\Html;
	use yii\widgets\LinkPager;
	use yii\bootstrap\ActiveForm;
	use ymaker\social\share\widgets\SocialShare;

	$this->title = 'View article';

?>
<div class="row">
	<!--Post and comments-->
	<div class="col-lg-8 col-md-7">
		<div class="card mb-4">
			<img src="<?=$currentArticle->getImage()?>" alt="" class="card-img-top">
			<div class="card-body">
				<h4 class="card-title"><?=Html::encode($currentArticle->title)?></h4>
				<p class="card-text">
					<?=Html::encode($currentArticle->content)?>
				</p>
				<div id="post-tags">
					<?php foreach ($currentArticle->tags as $tag):?>
						<span class="badge badge-info"><?=Html::encode($tag->title)?></span>
					<?php endforeach;?>
				</div>
			</div>
			<div class="container-fluid card-footer text-muted">
				<div class="row justify-content-between">
					<span class="ml-2">
						By <?=($currentArticle->author) ? Html::encode($currentArticle->author->name) : 'Unknown'?>, 
						<?=$currentArticle->getDate()?>
					</span>
					<span class="mr-2">
						<?= SocialShare::widget([
					    'configurator'  => 'socialShare',
					    'url'           => Url::to(['site/view', 'id' => $currentArticle->id], true),
					    'title'         => $currentArticle->title,
					    'description'   => $currentArticle->description,
					    'imageUrl'      => Url::to($currentArticle->getImage(), true),
					    'containerOptions' => ['tag' => 'span',
					    	'class' => 'mr-2'],
					    'linkContainerOptions' => ['tag' => 'span',
					    	'class' => 'mr-2']
						]); ?>
					</span>
				</div>
			</div>
		</div>
		<!--Comments-->
		<div id="post-comments">
			<?php if (!empty($comments)):?>
				<?php foreach ($comments as $comment):?>
					<div class="media rounded border p-1 mb-3 comment">
						<img src="<?=$comment->user->getImage()?>" alt="user-avatar" class="rounded img-fluid mr-2">
						<div class="media-body container-fluid">
							<h5 class="mt-1 row justify-content-between">
								<a href="#">
									<?=Html::encode($comment->user->name)?>
								</a>
								<span class="badge badge-secondary">
									<?=$comment->getDate()?>
								</span>
							</h5>
							<p class="row">
								<?=Html::encode($comment->text)?>
							</p>
						</div>
					</div>
				<?php endforeach;?>
			<?php else:?>
				<p>
					There is no comments for this article for now.
				</p>
			<?php endif;?>
		</div>
	</div>
	<?=$this->render('partials/sidebar', compact(
		'popularPosts', 'recentPosts', 'categories', 'tags'))?>

  <div class="col-12 pagination-block">
    <?=LinkPager::widget([
      'pagination' => $pagination,
      'activePageCssClass' => 'pagination-block__active',
      'linkOptions' => ['class' => 'pagination-block__link'],
      'disabledPageCssClass' => 'disabled'
    ])?>
  </div>

  <?=$this->render('partials/comment_form', compact(
  	'currentArticle', 'commentModel'))?>

</div>