<?php

use yii\db\Migration;

/**
 * Class m190926_055530_alter_text_column_in_comment_table
 */
class m190926_055530_alter_text_column_in_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%comment}}', 'text', $this->text());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%comment}}', 'text', $this->string());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190926_055530_alter_text_column_in_comment_table cannot be reverted.\n";

        return false;
    }
    */
}
