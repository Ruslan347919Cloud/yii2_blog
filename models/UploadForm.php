<?php
namespace app\models;

use yii\base\Model;

class UploadForm extends Model {

	public $imageFile;

	public function rules() {
		return [
			[['imageFile'],
			'file',
			'skipOnEmpty' => FALSE,
			'extensions' => 'jpg,png,gif']
		];
	}

	public function upload() {
		if ($this->validate()) {
			$this->imageFile
				->saveAs('uploads/'.$this->imageFile->baseName
				.'.'.$this->imageFile->extension);
			return TRUE;
		} else {
			return FALSE;
		}
	}
} 