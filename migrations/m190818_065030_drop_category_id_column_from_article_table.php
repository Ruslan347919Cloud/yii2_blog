<?php

use yii\db\Migration;

/**
 * Handles dropping columns from table `{{%article}}`.
 */
class m190818_065030_drop_category_id_column_from_article_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%article}}', 'category_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn('{{%article}}', 'category_id', $this->integer());
    }
}
