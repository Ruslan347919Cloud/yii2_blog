<!--Comment Form-->
<?php
	use yii\helpers\Html;
	use yii\bootstrap\ActiveForm;

	if (!Yii::$app->user->isGuest) {
		$form = ActiveForm::begin([
  		'id' => 'comment-form',
  		'action' => [
  			'site/comment',
  			'id' => $currentArticle->id
  		],
  		'options' => [
  			'class' => 'form-horizontal col-md-8 col-sm-12'
  		]
  	]);

  	echo $form->field($commentModel, 'text')
  		->textArea(['
  			placeholder' => 'Comment text...',
  			'rows' => 3,
  			'cols' => 6
  		])
  		->label('Leave your comment');

		echo Html::submitButton('Leave comment',
			['class' => 'btn btn-primary']);

  	ActiveForm::end();
	}
?>

<?php if (Yii::$app->session->hasFlash('comment')):?>
	<div class="alert alert-success alert-dismissible col-12 mt-2" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<?php echo Yii::$app->session->getFlash('comment'); ?>
	</div>
<?php endif;?>