<?php

use yii\db\Migration;

/**
 * Class m190926_072118_alter_status_column_in_comment_table
 */
class m190926_072118_alter_status_column_in_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%comment}}', 'status', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%comment}}', 'status', $this->integer());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190926_072118_alter_status_column_in_comment_table cannot be reverted.\n";

        return false;
    }
    */
}
