<?php
namespace app\models;

use Yii;
use yii\base\Model;

class SignupForm extends Model {
	public $name, $email, $password;

	public function rules() {
		return [
			[['name', 'email', 'password'], 'required'],
			[['name'], 'string'],
			[['email'], 'email'],
			[['email'], 'unique', 'targetClass' => 'app\models\User', 'targetAttribute' => 'email']
		];
	}

	private function sendEmailConfirm($user) {
  	$sent = $user->sendMail(
  		'email_confirm', 'Confirmation of registration',
  		['emailConfirmToken' => $user->email_confirm_token]
  	);
  	if (!$sent)
  		throw new \RuntimeException('Sending error.');
  }

	public function signup() {
		if ($this->validate()) {
			$user = new User;
			$user->attributes = $this->attributes;
			$user->email_confirm_token = Yii::$app->security->generateRandomString();
			$userIsCreated = $user->create();
			$this->sendEmailConfirm($user);
			return $userIsCreated;
		}
	}
}