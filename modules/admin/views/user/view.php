<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'format' => 'html',
                'label' => 'Photo',
                'value' => function($data) {
                    return Html::img($data->getImage(), ['width' => 150]);
                }
            ],
            'id',
            'name',
            'email:email',
            'password'
        ]
    ]) ?>

    <p>
        <?= Html::a('Update user data', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Set photo', ['set-photo', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php
            echo Html::a(
                'Delete this user',
                [
                    'delete',
                    'id' => $model->id
                ],
                [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post'
                    ]
                ]
            );
        ?>
    </p>

    <?php if (count($model->articles) > 0):?>
    <h3>My Articles</h3>
    <ul>
        <?php foreach ($model->articles as $article):?>
        <li>
            <a href="<?=Url::to([
                    '/admin/article/view',
                    'id' => $article->id
                ])?>">
                <?=Html::encode($article->title)?>
            </a>
        </li>
        <?php endforeach;?>
    </ul>
    <?php else:?>
        <p>This user have no articles.</p>
    <?php endif;?>
    <?= Html::a('Create article', ['/admin/article/create', 'id' => $model->id], ['class' => 'btn btn-success']) ?>

</div>
