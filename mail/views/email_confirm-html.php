<?php
	$confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['auth/signup-confirm', 'token' => $emailConfirmToken]);
?>
<p>
	Follow the link below to confirm your email:
</p>
<div>
	<a href="<?= $confirmLink ?>"><?= $confirmLink ?></a>
</div>