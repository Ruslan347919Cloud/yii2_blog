<?php

namespace app\models;
use yii\data\Pagination;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $title
 *
 * @property ArticleCategory[] $articleCategories
 * @property Article[] $articles
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticles()
    {
        return $this->hasMany(Article::className(), ['id' => 'article_id'])->viaTable('article_category', ['category_id' => 'id']);
    }

    public function getArticlesCount() {
        return $this->getArticles()->count();
    }

    public static function getAll() {
        return Category::find()->with('articles')
            ->orderBy('title')->all();
    }

    public static function getArticlesByCategory($id, $pageSize = 3) {
        $query = Article::find()
            ->joinWith('categories')
            ->where(['category.id' => $id]);
        $pagination = new Pagination([
            'defaultPageSize' => $pageSize,
            'totalCount' => $query->count()
        ]);
        $articles = $query
            ->offset($pagination->offset)
            ->limit($pagination->limit)->all();
        $data = [
            'articles' => $articles,
            'pagination' => $pagination
        ];
        return $data;
    }
}
