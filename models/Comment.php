<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comment".
 *
 * @property int $id
 * @property string $text
 * @property int $user_id
 * @property int $article_id
 * @property int $status
 *
 * @property Article $article
 * @property User $user
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'article_id'], 'integer'],
            [['status'], 'boolean'],
            [['text'], 'required'],
            [['text'], 'trim'],
            [['text'], 'string', 'max' => 3000],
            [['article_id'], 'exist', 'skipOnError' => true, 'targetClass' => Article::className(), 'targetAttribute' => ['article_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['status'], 'default', 'value' => false],
            [['putdate'], 'datetime', 'format' => 'php:Y-m-d H:i:s'],
            [['putdate'], 'default', 'value' => date('Y-m-d H:i:s')]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Text',
            'user_id' => 'User ID',
            'article_id' => 'Article ID',
            'status' => 'Status',
            'putdate' => 'Put Date',
        ];
    }

    public function __construct($articleId = null) {
        parent::init();
        $this->article_id = $articleId;
        $this->user_id = Yii::$app->user->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArticle()
    {
        return $this->hasOne(Article::className(), ['id' => 'article_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getDate() {
        return Yii::$app->formatter->format($this->putdate, 'datetime');
    }

    public static function getAllForThisArticle($id, $pageSize = 3) {
        $query = Comment::find()
            ->where(['article_id' => $id, 'status' => true]);
        $pagination = new \yii\data\Pagination([
            'defaultPageSize' => $pageSize,
            'totalCount' => $query->count()
        ]);
        $comments = $query
            ->with('user')
            ->orderBy(['putdate' => SORT_DESC])
            ->offset($pagination->offset)
            ->limit($pagination->limit)->all();
        $data = [
            'comments' => $comments,
            'pagination' => $pagination
        ];
        return $data;
    }

    public function changeStatus() {
        $this->status = !$this->status;
        return $this->save(false);
    }
}
